#include <stdio.h>
int main() {
    int x, y;
    printf("Enter x: ");
    scanf("%d", &x);
    printf("Enter y: ");
    scanf("%d", &y);
    x = x - y;
    y = x + y;
    x = y - x;
    printf("x = %d\n", x);
    printf("y = %d\n", y);
    return 0;
}
